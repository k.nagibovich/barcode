# BarCode

Генерация штрих-кода EAN12 и QR-кода из yandex cloud function

Функция возвращающая PNG изображение штрих-кода EAN13 или QR-кода. Штрих-код должен быть указан в параметре "bc" при GET запросе к функции. Для получения QR-кода данные для его формирования передаются в параметре "qr". Функция размещается в Yandex Cloud.

Работу функции можно посмотреть по адресу
https://d5d65nddl8t8sd8ab6ht.apigw.yandexcloud.net/?bc=123214567890
https://d5d65nddl8t8sd8ab6ht.apigw.yandexcloud.net/?qr=my_name_is_Boris
