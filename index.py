from barcode import EAN13
from barcode.writer import ImageWriter
from io import BytesIO
import base64
import qrcode


def handler(event, context):
    bc = event["queryStringParameters"].get("bc")
    qr = event["queryStringParameters"].get("qr")
    rv = BytesIO()
    if bc is not None:
        EAN13(bc, writer=ImageWriter()).write(rv)
    if qr is not None:
        img = qrcode.make(qr)
        img.save(rv, format="PNG")

    return {
        'statusCode': 200,
        "isBase64Encoded": True,
        "headers":  {'Content-Type': 'image/png'},
        'body': base64.encodebytes(rv.getvalue()).decode("utf-8")
    }
